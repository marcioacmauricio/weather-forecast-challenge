Este projeto é uma simples implementação de React com React Router, React Redux e Bootstrap.

Para a criação do repositório inicial foi utilizado "create-react-app", sendo que logo após o repositório foi ejetado com "npm run eject" para possibilitar um maior controle de demais configurações que pudessem ser necessárias. 

As etapas do projeto consistem em:

-Criação do Repositório;

-Implementação do Bootstrap 4.x;

-Implementação do React Router;

-Criação das Views do projeto;

-Implementação do React Redux;

-Refatoração do Código